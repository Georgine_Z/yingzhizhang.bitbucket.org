package hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        // Create an empty hash map
        HashMap<String, Integer> animal = new HashMap<>();

        // Add elements in the map
        animal.put("lion", 10);
        animal.put("tiger", 20);
        animal.put("rabbit", 50);

        // Print size and content
        System.out.println("The size of animal hash map is :- "
                + animal.size());
        System.out.println(animal);

        // isEmpty
        System.out.println(animal.isEmpty());

        // Check if a key is present and if present, print value
        if(animal.containsKey("tiger")) {
            Integer a = animal.get("tiger");
            System.out.println("value for key" + "\"tiger\"" + " is:-" + a);
        }

        // Use keySet to get the map
        Set<String> set = animal.keySet();
        for(String key: set) {
            System.out.println(key+" " + animal.get(key));
        }

        // Use entrySet to get the map
        for(Map.Entry<String, Integer> entry : animal.entrySet()) {
            System.out.println("Key=" + entry.getKey() + " Value=" + entry.getValue());
        }

    }

}

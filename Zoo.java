package Proj1;

import pri.*;

public class Zoo {
    public static void main(String[] args) {
        //initialize staff Georgine
        Staff staff = new Staff(1, "Georgine", "F", "Ticket seller", 5500.0);
        staff.show_BasicInfo();
        staff.Show_StaffInfo();
        staff.Pay_Raise(1000.0);
        staff.Change_Type("Manager");
        System.out.println("===================================================");

        //initialize ticket
        Ticket ticket = new Ticket(180.0, "Normal Ticket", 1);
        ticket.Show_Price();
        ticket.Upgrade("Student Ticket", 7);
        System.out.println("===================================================");

        //initialize visitor Pablo
        Visitor visitor = new Visitor("Pablo","M",24,183.5,1);
        visitor.show_BasicInfo();
        visitor.Show_VisitorInfo();
        visitor.check_price();
        System.out.println("===================================================");

        //initialize animal dog

        Animal dog = new Dog("XiXi","Black and White","2Ha",5,30);
        dog.show_AnimalInfo();
        dog.Age_Change(1);
        dog.Weight_Change(0.5);
        Dog d=(Dog)dog;
        d.eat();
        d.play();
        d.run();
        dog.show_AnimalInfo();
        System.out.println("===================================================");


        //initialize animal cat

        Animal cat = new Cat("TuoTuo","White","Ragdoll",2,2.8);
        cat.show_AnimalInfo();
        cat.Weight_Change(0.3);
        cat.Age_Change(2);
        Cat c=(Cat)cat;
        c.eat();
        c.play();
        c.run();
        cat.show_AnimalInfo();
        System.out.println("===================================================");



        //initialize animal tiger
        Animal tiger = new Tiger("TiTi","Brown","Manchurian Tiger",15,120);
        tiger.show_AnimalInfo();
        tiger.Weight_Change(3);
        tiger.Age_Change(3);
        Tiger t=(Tiger)tiger;
        t.eat();
        t.run();
        t.play();
        tiger.show_AnimalInfo();
        System.out.println("===================================================");



        //initialize animal giraffe
        Giraffe giraffe = new Giraffe("KiKi","Brown","Sika Deer",2,2.8);
        giraffe.show_AnimalInfo();
        giraffe.Weight_Change(3.2);
        giraffe.Age_Change(1);
        Giraffe g=(Giraffe)giraffe;
        g.eat();
        g.play();
        g.run();
        giraffe.show_AnimalInfo();



    }


}



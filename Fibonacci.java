package Proj1;

public class Fibonacci {

    public static void main(String[] args){
        System.out.println(fib_iterative(6));
        System.out.println(fib_recursive(6));
        System.out.println(fib_tail(6));

    }
    //In iterative way
    public static int fib_iterative(int index){
        if(index==1||index==2){
            return 1;
        }
        else {
            int f1=1;
            int f2=1;
            int f3=0;
            for (int i = 0; i < index - 2; i++) {
                f3=f1+f2;
                f1=f2;
                f2=f3;
            }
            return f3;
        }

    }
    //In recursive way
    public static int fib_recursive(int index){
        if(index==1||index==2){
            return 1;
        }
        else{
            return fib_recursive(index-1)+fib_recursive(index-2);

        }
    }
    //In tail recursive way

    public static int fib_tail(int index){

        return tailRecursion(index,1,1);

    }
    public static int tailRecursion(int index,int a,int b){
        if(index<3){
            return b;
        }
        return tailRecursion(index-1,b,a+b);
    }


}

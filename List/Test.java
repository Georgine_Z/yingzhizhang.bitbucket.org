package List;

public class Test {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        ListNode l= new ListNode();
        l.setData(1);
        l.setData(5);
        l.setData(15);
        l.setData(4);
        System.out.println("The list size is:"+l.size());
        System.out.println("The third data is:"+l.getData(3));
        l.remove(3);
        System.out.println("The third data is:"+l.getData(3));
        System.out.println("The list size is:"+l.size());
        l.clearAllElements();
    }
}

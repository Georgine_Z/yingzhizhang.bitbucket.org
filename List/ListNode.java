package List;

public class ListNode {
    public Node first;
    public Node last;
    public int size = 0;



    public int getData(int index) {
        Node count = first;
        for(int i=1;i<index;i++) {
            count = count.next;
        }
        return count.data;
    }

    public void setData(int index){
        if(first == null) {
            first = new Node(index);
            last = first;
        }
        else {
            Node newA = new Node(index);
            last.next = newA;
            newA.previous = last;
            last = newA;
        }
        size ++;
    }


    public int size() {
        return size;
    }

    //remove the data of the entered position
    public void remove(int index) {
        Node temp = first;
        if(index == 1) {
            first = first.next;
            temp = null;
        }
        else {
            for(int i=1;i<index;i++) {
                temp = temp.next;
            }
            temp.next.previous = temp.previous;
            temp.previous.next = temp.next;
            temp = null;
        }
        size --;
    }

    //clear all the elements;
    public void clearAllElements() {
        first = null;
        last = null;
        size = 0;
    }
}

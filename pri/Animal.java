package pri;

public class Animal {
    private String name;
    private String color;
    private String species;
    private int old;
    private double weight;

    public Animal(String name, String color, String species, int old, double weight) {
        this.name = name;
        this.color = color;
        this.species = species;
        this.old = old;
        this.weight = weight;
    }

    public void Age_Change(int value) {
        old = old + value;
    }

    public void show_AnimalInfo() {
        System.out.println("Name:" + name + ",Color:" + color + ",Species:" + species + ",Old:" + old + ",Weight:" + weight);
    }

    public void Weight_Change(double value) {
        weight = weight + value;
    }
}

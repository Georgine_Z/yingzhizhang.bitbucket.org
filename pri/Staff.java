package pri;

public class Staff extends People {

    private int staff_id;
    private String staff_type;
    private String name;
    private double salary;

    public Staff(int staff_id, String name, String sex, String staff_type, double salary) {
        super(name, sex);
        this.staff_id = staff_id;
        this.staff_type = staff_type;
        this.salary = salary;
    }

    public void Show_StaffInfo() {
        System.out.println("name:" + name + ",staff_id:" + staff_id + ",staff_type:" + staff_type);
    }//Print staff's information.

    public void Pay_Raise(double money) {

        salary = salary + money;
    }//Staff's salary raises.

    public void Change_Type(String type) {

        staff_type = type;
    }//Staff changes work type.
}

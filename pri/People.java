package pri;

public class People {
    private String name;
    private String sex;

    public People(String name, String sex) {
        this.name = name;
        this.sex = sex;
    }

    public void show_BasicInfo() {
        System.out.println("name:" + name + ",sex:" + sex);
    }

}

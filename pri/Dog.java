package pri;

public class Dog extends Animal {

    public Dog(String name, String color, String species, int old, double weight) {
        super(name, color, species, old, weight);
    }


    public void eat() {
        System.out.println("dog is eating");
    }//Describe the behavior of eat.


    public void play() {
        System.out.println("dog is playing");
    }//Describe the behavior of play.


    public void run() {
        System.out.println("dog is running");
    }//Describe the behavior of run.

}

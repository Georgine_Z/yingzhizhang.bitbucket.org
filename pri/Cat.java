package pri;

public class Cat extends Animal {

    public Cat(String name, String color, String species, int old, double weight) {
        super(name, color, species, old, weight);
    }


    public void eat() {
        System.out.println("cat is eating");
    }//Describe the behavior of eat.


    public void play() {
        System.out.println("cat is playing");
    }//Describe the behavior of play.


    public void run() {
        System.out.println("cat is running");
    }//Describe the behavior of run.

}

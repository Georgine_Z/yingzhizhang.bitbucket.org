package pri;

public class Visitor extends People {
    private int age;
    private double height;
    private int amount;

    public Visitor(String name, String sex, int age, double height, int amount) {
        super(name, sex);
        this.age = age;
        this.height = height;
        this.amount = amount;
    }

    public void Show_VisitorInfo() {
        System.out.println("Age:" + age + ",Height:" + height + ",Amount:" + amount);
    }

    public void check_price() {
        if (age < 12 && height < 1.2) {
            System.out.println("Student price");
        } else if (age > 60) {
            System.out.println("Elder price");
        } else {
            System.out.println("Normal price");
        }
    }//Different type of visitors have different price.
}

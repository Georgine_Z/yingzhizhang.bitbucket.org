package pri;

public class Tiger extends Animal {

    public Tiger(String name, String color, String species, int old, double weight) {
        super(name, color, species, old, weight);
    }


    public void eat() {
        System.out.println("tiger is eating");
    }//Describe the behavior of eat.


    public void play() {
        System.out.println("tiger is playing");
    }//Describe the behavior of play.


    public void run() {
        System.out.println("tiger is running");
    }//Describe the behavior of run.

}

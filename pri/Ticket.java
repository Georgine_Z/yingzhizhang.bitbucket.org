package pri;

public class Ticket {
    private double price;
    private String ticket_type;
    private int valid_day;

    public Ticket(double price, String ticket_type, int valid_day) {
        this.price = price;
        this.ticket_type = ticket_type;
        this.valid_day = valid_day;
    }

    public void Show_Price() {

        System.out.println("price:" + price);
    }//Print the price of ticket.

    public void Upgrade(String ticket_new, int new_day) {

        ticket_type = ticket_new;
        valid_day = new_day;
    }//Upgrade ticket.
}

package pri;

public class Giraffe extends Animal {

    public Giraffe(String name, String color, String species, int old, double weight) {
        super(name, color, species, old, weight);
    }


    public void eat() {
        System.out.println("giraffe is eating");
    }//Describe the behavior of eat.


    public void play() {
        System.out.println("giraffe is playing");
    }//Describe the behavior of play.


    public void run() {
        System.out.println("giraffe is running");
    }//Describe the behavior of run.

}

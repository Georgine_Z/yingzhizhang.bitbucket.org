package hashtable;

public class HashTable {

    private final int CAPACITY = 15;
    private final int InitialValue = -9999;

    private int[] arr = new int[CAPACITY];
    private int size = 0;

    //Insert method
    public void put(int key) {
        int i = 0;
        if(size == 0) {
            InitHashTable();
        }
        while(arr[Hash(key+i)] != InitialValue) {
            i++;
        }
        arr[Hash(key)+i] = key;
        size++;
    }


    // Get method
    public int get(int number) {
        return arr[number];
    }

    // Clear method
    public void clear() {
        InitHashTable();
        size =0;
    }

    // Size method
    public int size() {
        return size;
    }

    // Initialize the hash table
    private void InitHashTable() {
        for(int i = 0; i<CAPACITY; i++) {
            arr[i] = InitialValue;
        }
    }

    // Show the HashTable
    public void show() {
        for(int i=0; i<CAPACITY; i++) {
            if(arr[i] != InitialValue) {
                System.out.println(i + "\t" + "key:" + arr[i] );
            }
        }
    }

    // method for store the key
    private int Hash(int key) {
        return key % CAPACITY;
    }
}
